﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;

namespace Cassandra_GreatReads.Models
{
	public class OceneKorisnika
    {
		public int ID_Knjige;
		public int ID_Korisnika;
		public int Ocena;
    }
	public class OceneKorisnikaData
    {
		public List<OceneKorisnika> Ocene = new List<OceneKorisnika>();

		public OceneKorisnikaData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select * from ocenekorisnika");
                OceneKorisnika tmpObj = null;

                results.OrderBy(x => x.GetColumn("id_knjige"));
                foreach(var result in results)
                {
                    tmpObj = new OceneKorisnika();

                    tmpObj.ID_Knjige = result.GetValue<int>("id_knjige");
                    tmpObj.ID_Korisnika = result.GetValue<int>("id_korisnika");
                    tmpObj.Ocena = result.GetValue<int>("ocena");

                    Ocene.Add(tmpObj);
                }
            }
            catch(Exception ex)
            {

            }
        }
    }
}

