﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;

namespace Cassandra_GreatReads.Models
{
    public class Korisnik
    {
        public int ID_Korisnika { get; set; }
        public string Email { get; set; }
        public LocalDate God_rodjenja { get; set; }
        public string Ime_i_prezime { get; set; }
        public string Lozinka { get; set; }
        public string Pol { get; set; }

        public string Username { get; set; }

    }

    public class KorisnikData
    {
        public IList<Korisnik> Korisnici = new List<Korisnik>();

        public KorisnikData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select * from korisnik");
                Korisnik kor = null;

                foreach(var rez in results)
                {
                    kor = new Korisnik();
                    kor.ID_Korisnika = rez.GetValue<int>("id_korisnika");
                    kor.Email = rez.GetValue<string>("email");
                    kor.God_rodjenja = rez.GetValue<LocalDate>("godina_rodjenja");
                    kor.Ime_i_prezime = rez.GetValue<string>("ime_i_prezime");
                    kor.Lozinka = rez.GetValue<string>("lozinka");
                    kor.Pol = rez.GetValue<string>("pol");
                    kor.Username = rez.GetValue<string>("username");

                    Korisnici.Add(kor);
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }



}
