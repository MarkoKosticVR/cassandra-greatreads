﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;
using Cassandra_GreatReads.Controllers;

namespace Cassandra_GreatReads.Models
{
    public class Autor
    {
        public int ID_Autora;
        public String Ime_Prezime;
        public LocalDate Godina_rođena;
        public LocalDate Godina_smrti;
        public String Zemlja;
        public String Biografija;
        public int Broj_knjiga;
        public float Prosecna_ocena;
    }
    public class AutorData
    {
        public List<Autor> Autori = new List<Autor>();

        public AutorData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select * from autor");
                Autor tmpObj = null;

                foreach (var result in results)
                {
                    tmpObj = new Autor();

                    tmpObj.ID_Autora = result.GetValue<int>("id_autora");
                    tmpObj.Ime_Prezime = result.GetValue<String>("ime_i_prezime");
                    tmpObj.Godina_rođena = result.GetValue<LocalDate>("godina_rodjenja");
                    tmpObj.Godina_smrti = result.GetValue<LocalDate>("godina_smrti");
                    tmpObj.Zemlja = result.GetValue<String>("zemlja_porekla");
                    tmpObj.Biografija = result.GetValue<String>("biografija");
                    tmpObj.Broj_knjiga = result.GetValue<int>("broj_knjiga");
                    tmpObj.Prosecna_ocena = result.GetValue<float>("prosecna_ocena");

                    Autori.Add(tmpObj);
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
