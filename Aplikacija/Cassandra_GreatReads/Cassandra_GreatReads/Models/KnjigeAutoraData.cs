﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;

namespace Cassandra_GreatReads.Models
{
    public class KnjigaAutora
    {
        public int ID_Knjige;
        public int ID_Autora;
        public String Naslov;
    }
    public class KnjigeAutoraData
    {
        public List<KnjigaAutora> Ocene = new List<KnjigaAutora>();

        public KnjigeAutoraData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select * from knjigeautora;");
                KnjigaAutora tmpObj = null;

                foreach (var result in results)
                {
                    tmpObj = new KnjigaAutora();

                    tmpObj.ID_Knjige = result.GetValue<int>("id_knjige");
                    tmpObj.ID_Autora = result.GetValue<int>("id_autora");
                    tmpObj.Naslov = result.GetValue<String>("naslov");

                    Ocene.Add(tmpObj);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}

