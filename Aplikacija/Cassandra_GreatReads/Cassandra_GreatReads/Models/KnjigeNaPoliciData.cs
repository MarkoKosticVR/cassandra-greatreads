﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;

namespace Cassandra_GreatReads.Models
{
    public class KnjigeNaPolici
    {
        public Polica polica;
        public List<Knjiga> knjige;
    }
    public class KnjigeNaPoliciData
    {
        public List<KnjigeNaPolici> police = new List<KnjigeNaPolici>();

        public KnjigeNaPoliciData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select distinct id_police from knjigenapolici;").Distinct();

                KnjigeNaPolici tmpObj = null;
                Polica tmpPolica = null;
                List<Knjiga> tmpKnjige = null;
                Knjiga tmpKnjiga = null;

                foreach (var result in results)
                {
                    tmpObj = new KnjigeNaPolici();
                    tmpPolica = new Polica();
                    //izvlacim policu
                    int id_police = result.GetValue<int>("id_police");

                    var sp = session.Execute("select * from polica where id_police=" + id_police + ";").First();

                    tmpPolica.ID_Police = sp.GetValue<int>("id_police");
                    tmpPolica.Naziv = sp.GetValue<String>("naziv");
                    tmpPolica.Broj_knjiga = sp.GetValue<int>("broj_knjiga");

                    tmpObj.polica = tmpPolica;

                    var skp = session.Execute("select id_knjige from knjigenapolici where id_police=" + id_police + ";");

                    tmpKnjige = new List<Knjiga>();

                    foreach (var sk in skp)
                    {
                        tmpKnjiga = new Knjiga();
                        int id_knjige = sk.GetValue<int>("id_knjige");


                        var knjiga = session.Execute("select * from knjiga where id_knjige=" + id_knjige + ";").FirstOrDefault();

                        tmpKnjiga.ID_Knjige = id_knjige;
                        tmpKnjiga.Naslov = knjiga.GetValue<String>("naslov");
                        tmpKnjiga.Naslov_originala = knjiga.GetValue<String>("naslov_originala");
                        tmpKnjiga.Autor = knjiga.GetValue<String>("autor");
                        tmpKnjiga.Broj_stranica = knjiga.GetValue<int>("broj_stranica");
                        tmpKnjiga.Format_knjige = knjiga.GetValue<String>("format_knjige");
                        tmpKnjiga.Godina_izdavanja = knjiga.GetValue<LocalDate>("godina_izdavanja");
                        tmpKnjiga.Jezik = knjiga.GetValue<String>("jezik");
                        tmpKnjiga.Prosecna_ocena = knjiga.GetValue<float>("prosecna_ocena");
                        tmpKnjiga.Zanr = knjiga.GetValue<String>("zanr");

                        tmpKnjige.Add(tmpKnjiga);
                    }

                    tmpObj.knjige = tmpKnjige;

                    police.Add(tmpObj);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public KnjigeNaPoliciData(int id_police)
        {
            var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
            using var session = cluster.Connect("greatreads");
            var results = session.Execute("select id_knjige from knjigenapolici where id_police=" + id_police + ";");

            KnjigeNaPolici tmpObj = new KnjigeNaPolici();
            Polica tmpPolica = new Polica();
            List<Knjiga> tmpKnjige = new List<Knjiga>();
            Knjiga tmpKnjiga = null;

            var sp = session.Execute("select * from polica where id_police=" + id_police + ";").First();

            tmpPolica.ID_Police = sp.GetValue<int>("id_police");
            tmpPolica.Naziv = sp.GetValue<String>("naziv");
            tmpPolica.Broj_knjiga = sp.GetValue<int>("broj_knjiga");

            tmpObj.polica = tmpPolica;

            foreach (var knjige in results)
            {
                tmpKnjiga = new Knjiga();
                int id_knjige = knjige.GetValue<int>("id_knjige");


                var knjiga = session.Execute("select * from knjiga where id_knjige=" + id_knjige + ";").FirstOrDefault();

                tmpKnjiga.ID_Knjige = id_knjige;
                tmpKnjiga.Naslov = knjiga.GetValue<String>("naslov");
                tmpKnjiga.Naslov_originala = knjiga.GetValue<String>("naslov_originala");
                tmpKnjiga.Autor = knjiga.GetValue<String>("autor");
                tmpKnjiga.Broj_stranica = knjiga.GetValue<int>("broj_stranica");
                tmpKnjiga.Format_knjige = knjiga.GetValue<String>("format_knjige");
                tmpKnjiga.Godina_izdavanja = knjiga.GetValue<LocalDate>("godina_izdavanja");
                tmpKnjiga.Jezik = knjiga.GetValue<String>("jezik");
                tmpKnjiga.Prosecna_ocena = knjiga.GetValue<float>("prosecna_ocena");
                tmpKnjiga.Zanr = knjiga.GetValue<String>("zanr");

                tmpKnjige.Add(tmpKnjiga);
            }

            tmpObj.knjige = tmpKnjige;

            police.Add(tmpObj);
        }
    }
}
