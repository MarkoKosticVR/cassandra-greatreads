﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;

namespace Cassandra_GreatReads.Models
{
    public class Polica
    {
        public int ID_Police;
        public String Naziv;
        public int Broj_knjiga;
    }
    public class PolicaData
    {
        public List<Polica> Police = new List<Polica>();
        public int id_knjige;
        public PolicaData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select * from polica");
                Polica tmpObj = null;

                foreach (var result in results)
                {
                    tmpObj = new Polica();

                    tmpObj.ID_Police = result.GetValue<int>("id_police");
                    tmpObj.Naziv = result.GetValue<String>("naziv");
                    tmpObj.Broj_knjiga = result.GetValue<int>("broj_knjiga");

                    Police.Add(tmpObj);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public PolicaData(int id_korisnika)
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var ids = session.Execute("select id_police from policekorisnika where id_korisnika=" + id_korisnika + ";");
                Polica tmpObj = null;

                foreach (var id in ids)
                {
                    var police = session.Execute("select * from polica where id_police=" + id.GetValue<int>(0) + ";");
                    foreach (var polica in police)
                    {
                        tmpObj = new Polica();

                        tmpObj.ID_Police = polica.GetValue<int>("id_police");
                        tmpObj.Naziv = polica.GetValue<String>("naziv");
                        tmpObj.Broj_knjiga = polica.GetValue<int>("broj_knjiga");

                        Police.Add(tmpObj);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
