﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra;

namespace Cassandra_GreatReads.Models
{
    public class Knjiga
    {
        public int ID_Knjige;
        public String Naslov;
        public String Naslov_originala;
        public String Autor;
        public String Format_knjige;
        public String Jezik;
        public String Zanr;
        public int Broj_stranica;
        public float Prosecna_ocena;
        public LocalDate Godina_izdavanja;
    }
    public class KnjigaData
    {
        public List<Knjiga> Knjige = new List<Knjiga>();

        public KnjigaData()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                using var session = cluster.Connect("greatreads");
                var results = session.Execute("select * from knjiga");
                Knjiga tmpObj = null;

                foreach (var result in results)
                {
                    tmpObj = new Knjiga();

                    tmpObj.ID_Knjige = result.GetValue<int>("id_knjige");
                    tmpObj.Naslov = result.GetValue<String>("naslov");
                    tmpObj.Naslov_originala = result.GetValue<String>("naslov_originala");
                    tmpObj.Autor = result.GetValue<String>("autor");
                    tmpObj.Broj_stranica = result.GetValue<int>("broj_stranica");
                    tmpObj.Format_knjige = result.GetValue<String>("format_knjige");
                    tmpObj.Godina_izdavanja = result.GetValue<LocalDate>("godina_izdavanja");
                    tmpObj.Jezik = result.GetValue<String>("jezik");
                    tmpObj.Prosecna_ocena = result.GetValue<float>("prosecna_ocena");
                    tmpObj.Zanr = result.GetValue<String>("zanr");

                    Knjige.Add(tmpObj);
                }
            }
            catch(Exception ex)
            {
            }
        }
    }
}
