﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra_GreatReads.Models;
using Cassandra;

namespace Cassandra_GreatReads.Controllers
{
    public class PolicaController : Controller
    {
        ISession session = null;

        public PolicaController()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                session = cluster.Connect("greatreads");
            }
            catch(Exception ex)
            { }
        }
        public IActionResult DisplayPolicaData(int id_korisnika)
        {
            PolicaData pd = null;
            if(id_korisnika==0)
                pd = new PolicaData();
            else
                pd = new PolicaData(id_korisnika);
            return View(pd);
        }

        public IActionResult DisplayPolica(int id_police)
        {
            if (id_police != 0)
            {
                KnjigeNaPoliciData kp = new KnjigeNaPoliciData(id_police);
                return View(kp);
            }
            else
                return RedirectToAction("DisplayPolicaData");
        }

        public IActionResult GoToCreatePolica(int id_police)
        {
            if (id_police != 0)
            {
                Polica tmpObj = null;

                try
                {
                    var results = session.Execute("SELECT * FROM polica WHERE " +
                                    "id_police =" + id_police + " ; ");

                    foreach (var result in results)
                    {
                        tmpObj = new Polica();

                        tmpObj.ID_Police = result.GetValue<int>("id_police");
                        tmpObj.Naziv = result.GetValue<String>("naziv");
                        tmpObj.Broj_knjiga = result.GetValue<int>("broj_knjiga");
                    }
                }
                catch (Exception ex) { }

                return View(tmpObj);
            }
            else
                return View();
        }

        private Boolean CheckPolica(Polica polica)
        {
            if (polica.ID_Police == 0)
                return false;
            return true;
        }

        public IActionResult SavePolica(int id_police, String naziv, int id_korisnika)
        {
            int broj_knjiga = 0;
            try
            {
                var result = session.Execute("Select * from polica where id_police=" + id_police + ";");
                if (result.Count() != 0)
                    broj_knjiga = result.First().GetValue<int>("broj_knjiga");
            }
            catch(Exception ex)
            { }

            Polica kObj = new Polica();

            kObj.ID_Police = id_police;
            kObj.Naziv = naziv;
            kObj.Broj_knjiga = broj_knjiga;

            if (!CheckPolica(kObj))
                return RedirectToAction("DisplayPolicaData");

            var pk = session.Execute("select * from policekorisnika where id_police=" +
                                        id_police + " and id_korisnika=" + id_korisnika + ";");

            if(pk.Count()!=0)
                return RedirectToAction("DisplayPolicaData");

            try
            {
                session.Execute("INSERT INTO polica (id_police, naziv, broj_knjiga)" +
                                "VALUES (" + kObj.ID_Police +
                                ", \'" + kObj.Naziv + "\'" +
                                ", " + kObj.Broj_knjiga + ");");

                session.Execute("Insert into policekorisnika (id_korisnika, id_police) " +
                                "values (" + id_korisnika + ", " + id_police + ");");
            }
            catch (Exception ex) { }
            return RedirectToAction("DisplayPolicaData");
        }

        public IActionResult DeletePolica(int id_police)
        {
            try
            {
                session.Execute("delete from knjigenapolici where id_police=" + id_police + ";");
                var ids = session.Execute("select * from policekorisnika where id_police=" + id_police + ";");

                foreach(var id in ids)
                {
                    int id_korisnika = id.GetValue<int>("id_korisnika");

                    session.Execute("delete from policekorisnika where id_police = " + id_police +
                                    " and id_korisnika=" + id_korisnika + "; ");
                }

                session.Execute("DELETE FROM polica WHERE " +
                                "id_police =" + id_police + " ; ");                
            }
            catch (Exception ex) { }

            return RedirectToAction("DisplayPolicaData");
        }

        public IActionResult UpdatePolica(int id_police)
        {
            return RedirectToAction("GoToCreatePolica", new { id_police = id_police });
        }

        public IActionResult GoToDodajNaPolicu(int id_knjige)
        {
            PolicaData pd = new PolicaData();
            pd.id_knjige = id_knjige;
            return View(pd);
        }

        public IActionResult DodajNaPolicu(int id_knjige, int id_police)
        {
            if(id_knjige!=0 && id_police!=0)
            {
                var kp = session.Execute("select * from knjigenapolici where id_police=" + id_police +
                                        " and id_knjige=" + id_knjige + ";");

                if(kp.Count() != 0)
                    return RedirectToAction("DisplayPolicaData");
                try
                {
                    session.Execute("Insert into knjigenapolici (id_police, id_knjige)" +
                                    "values (" + id_police + ", " + id_knjige + ");");
                    var result = session.Execute("select * from polica where id_police=" + id_police + ";").First();
                    Polica p = new Polica();

                    p.Broj_knjiga = result.GetValue<int>("broj_knjiga");
                    p.Broj_knjiga++;

                    session.Execute("Update polica set broj_knjiga=" + p.Broj_knjiga + " where id_police=" + id_police + ";");
                }
                catch(Exception ex)
                { }
            }
            return RedirectToAction("DisplayPolicaData");
        }

        public IActionResult UkloniKnjigu(int id_police, int id_knjige)
        {
            int id_korisnika = 0;
            try
            {
                session.Execute("delete from knjigenapolici where id_police=" + id_police + " and id_knjige=" + id_knjige + ";");
                var polica = session.Execute("select * from polica where id_police=" + id_police + ";").First();

                Polica p = new Polica();

                p.Broj_knjiga = polica.GetValue<int>("broj_knjiga");
                p.Broj_knjiga--;

                session.Execute("update polica set broj_knjiga=" + p.Broj_knjiga + " where id_police=" + id_police+";");

                var korisnik = session.Execute("select id_korisnika from policekorisnika where id_police=" + id_police + ";").First();

                id_korisnika = korisnik.GetValue<int>(0);
            }
            catch(Exception ex)
            { }
            return RedirectToAction("DisplayPolicaData", new { id_korisnika = id_korisnika });
        }
    }
}
