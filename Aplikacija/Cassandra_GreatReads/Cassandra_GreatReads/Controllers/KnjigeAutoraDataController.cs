﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra_GreatReads.Models;
using Cassandra;
using System.Globalization;

namespace Cassandra_GreatReads.Controllers
{
    public class KnjigeAutoraDataController : Controller
    {
        ISession session = null;

        public KnjigeAutoraDataController()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                session = cluster.Connect("greatreads");
            }
            catch (Exception ex)
            { }
        }
        public IActionResult DisplayKnjigeAutoraData()
        {
            KnjigeAutoraData okd = new KnjigeAutoraData();
            return View(okd);
        }

        public Boolean CheckKnjiga(KnjigaAutora KnjigaAutora)
        {
            if (KnjigaAutora.ID_Knjige == 0 || KnjigaAutora.ID_Autora == 0)
                return false;
            return true;
        }

        public IActionResult DeleteKnjigaAutora(int id_knjige, int id_autora)
        {
            try
            {
                session.Execute("DELETE FROM knjigeautora WHERE " +
                                "id_knjige =" + id_knjige + " and " +
                                "id_autora =" + id_autora + ";");
            }
            catch (Exception ex)
            {

            }

            int broj_knjiga;

            var bk = session.Execute("Select broj_knjiga from autor where id_autora=" + id_autora + ";");
            broj_knjiga = bk.First().GetValue<int>(0);
            broj_knjiga--;

            session.Execute("update autor set broj_knjiga=" + broj_knjiga + " where id_autora=" + id_autora + ";");

            return RedirectToAction("DisplayKnjigeAutoraData");
        }

        public IActionResult GoToCreateKnjigaAutora(int id_knjige, int id_autora)
        {
            if (id_knjige != 0 && id_autora != 0)
            {
                KnjigaAutora tmpObj = null;

                try
                {
                    var results = session.Execute("SELECT * FROM knjigeautora WHERE " +
                                  "id_knjige =" + id_knjige + " and " +
                                  "id_autora = " + id_autora + ";");

                    foreach (var result in results)
                    {
                        tmpObj = new KnjigaAutora();

                        tmpObj.ID_Knjige = result.GetValue<int>("id_knjige");
                        tmpObj.ID_Autora = result.GetValue<int>("id_autora");
                        tmpObj.Naslov = result.GetValue<String>("naslov");
                    }
                }
                catch (Exception ex)
                {

                }

                return View(tmpObj);
            }

            return View();
        }

        public IActionResult SaveKnjigaAutora(int id_knjige, int id_autora)
        {

            KnjigaAutora oObj = new KnjigaAutora();

            oObj.ID_Knjige = id_knjige;
            oObj.ID_Autora = id_autora;

            if (!CheckKnjiga(oObj))
                return RedirectToAction("DisplayKnjigeAutoraData");

            var knjiga = session.Execute("select naslov from knjiga where id_knjige=" + id_knjige + ";").First();

            oObj.Naslov = knjiga.GetValue<String>(0);

            try
            {
                session.Execute("INSERT INTO knjigeautora (id_knjige, id_autora, naslov) " +
                    "VALUES (" + oObj.ID_Knjige +
                    ", " + oObj.ID_Autora +
                    ", \'" + oObj.Naslov + "\');");
            }
            catch (Exception ex)
            {

            }

            int broj_knjiga;

            var bk = session.Execute("Select broj_knjiga from autor where id_autora=" + id_autora + ";");
            broj_knjiga = bk.First().GetValue<int>(0);
            broj_knjiga++;

            session.Execute("update autor set broj_knjiga=" + broj_knjiga + " where id_autora=" + id_autora + ";");

            return RedirectToAction("DisplayKnjigeAutoraData");
        }

    }
}