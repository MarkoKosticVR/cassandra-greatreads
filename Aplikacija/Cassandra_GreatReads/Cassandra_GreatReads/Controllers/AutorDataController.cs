﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra_GreatReads.Models;
using Cassandra;
using System.Globalization;


namespace Cassandra_GreatReads.Controllers
{
    public class AutorDataController : Controller
    {
        ISession session = null;

        public AutorDataController()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                session = cluster.Connect("greatreads");
            }
            catch (Exception ex)
            { }
        }
        public IActionResult DisplayAutorData()
        {
            AutorData ad = new AutorData();
            return View(ad);
        }

        public IActionResult GoToCreateAutor(int id_autora)
        {
            if (id_autora != 0)
            {
                Autor tmpObj = null;

                try
                {
                    var results = session.Execute("SELECT * FROM autor WHERE " +
                                    "id_autora =" + id_autora + " ; ");

                    foreach (var result in results)
                    {
                        tmpObj = new Autor();

                        tmpObj.ID_Autora = result.GetValue<int>("id_autora");
                        tmpObj.Ime_Prezime = result.GetValue<String>("ime_i_prezime");
                        tmpObj.Godina_rođena = result.GetValue<LocalDate>("godina_rodjenja");
                        tmpObj.Godina_smrti = result.GetValue<LocalDate>("godina_smrti");
                        tmpObj.Zemlja = result.GetValue<String>("zemlja_porekla");
                        tmpObj.Biografija = result.GetValue<String>("biografija");
                        tmpObj.Broj_knjiga = result.GetValue<int>("broj_knjiga");
                        tmpObj.Prosecna_ocena = result.GetValue<float>("prosecna_ocena");
                    }
                }
                catch (Exception ex) { }

                return View(tmpObj);
            }
            else
                return View();
        }

        private Boolean CheckAutor(Autor autor)
        {
            if (autor.ID_Autora == 0)
                return false;
            return true;
        }

        public IActionResult SaveAutor(int id_autora, String ime_prezime, DateTime godina_rođenja, DateTime godina_smrti,
            String zemlja, int broj_knjiga, float prosecna_ocena, String biografija)
        {
            Autor kObj = new Autor();

            kObj.ID_Autora = id_autora;
            kObj.Ime_Prezime = ime_prezime;
            kObj.Godina_rođena = new LocalDate(godina_rođenja.Year,godina_rođenja.Month,godina_rođenja.Day);
            kObj.Godina_smrti = new LocalDate(godina_smrti.Year, godina_smrti.Month, godina_smrti.Day);
            kObj.Zemlja = zemlja;
            kObj.Broj_knjiga = broj_knjiga;
            kObj.Biografija = biografija;
            kObj.Prosecna_ocena = prosecna_ocena;

            if (!CheckAutor(kObj))
                return RedirectToAction("DisplayAutorData");

            LocalDate localDate = null;
            LocalDate minLD = new LocalDate(1, 1, 1);
            if (kObj.Godina_smrti == minLD)
                localDate = new LocalDate(9999, 12, 31);
            else
                localDate = new LocalDate(kObj.Godina_smrti.Year, kObj.Godina_smrti.Month, kObj.Godina_smrti.Day);

            LocalDate localDate1 = null;
            LocalDate minLD1 = new LocalDate(1, 1, 1);
            if (kObj.Godina_rođena == minLD1)
                localDate1 = new LocalDate(1000, 10, 10);
            else
                localDate1 = new LocalDate(kObj.Godina_rođena.Year, kObj.Godina_rođena.Month, kObj.Godina_rođena.Day);


            try
            {
                session.Execute("INSERT INTO autor (id_autora, ime_i_prezime, godina_rodjenja, godina_smrti, zemlja_porekla," +
                                "broj_knjiga, prosecna_ocena, biografija)" +
                                "VALUES (" + kObj.ID_Autora +
                                ", \'" + kObj.Ime_Prezime + "\'" +
                                ", \'" + localDate1.ToString() + "\'" +
                                ", \'" + localDate.ToString() + "\'" +
                                ", \'" + kObj.Zemlja + "\'" +
                                ", " + kObj.Broj_knjiga +
                                ", " + kObj.Prosecna_ocena +
                                ", \'" + kObj.Biografija + "\');");
            }
            catch (Exception ex) { }
            return RedirectToAction("DisplayAutorData");
        }

        public IActionResult DeleteAutor(int id_autora)
        {
            try
            {
                session.Execute("DELETE FROM autor WHERE " +
                                "id_autora =" + id_autora + " ; ");
            }
            catch (Exception ex) { }

            return RedirectToAction("DisplayAutorData");
        }

        public IActionResult UpdateAutor(int id_autora)
        {
            return RedirectToAction("GoToCreateAutor", new { id_autora = id_autora });
        }

        public IActionResult DodajKnjiguAutoru(int id_autora)
        {
            return View(id_autora);
        }
    }
}
