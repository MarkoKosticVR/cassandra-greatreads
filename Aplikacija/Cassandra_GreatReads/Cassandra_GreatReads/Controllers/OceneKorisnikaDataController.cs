﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra_GreatReads.Models;
using Cassandra;
using System.Globalization;

namespace Cassandra_GreatReads.Controllers
{
    public class OceneKorisnikaDataController : Controller
    {
        ISession session = null;

        public OceneKorisnikaDataController()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                session = cluster.Connect("greatreads");
            }
            catch (Exception ex)
            { }
        }
        public IActionResult DisplayOceneKorisnikaData()
        {
            OceneKorisnikaData okd = new OceneKorisnikaData();
            return View(okd);
        }

        public Boolean CheckOcena(OceneKorisnika ocena)
        {
            if (ocena.ID_Knjige == 0 || ocena.ID_Korisnika == 0)
                return false;
            return true;
        }

        public IActionResult DeleteOcena(int id_knjige, int id_korisnika)
        {
            try
            {
                session.Execute("DELETE FROM ocenekorisnika WHERE " +
                                "id_knjige =" + id_knjige + " and " +
                                "id_korisnika =" + id_korisnika + ";");
            }
            catch(Exception ex)
            {

            }
            RacunajProsekOcena(id_knjige);
            RacunajProsekAutora(id_knjige);
            return RedirectToAction("DisplayOceneKorisnikaData");
        }

        public IActionResult GoToCreateOcena(int id_knjige, int id_korisnika, int ocena)
        {
            OceneKorisnika tmpObj = null;

            tmpObj = new OceneKorisnika();

            tmpObj.ID_Knjige = id_knjige;
            tmpObj.ID_Korisnika = id_korisnika;
            tmpObj.Ocena = ocena;

            return View(tmpObj);
        }

        public IActionResult SaveOcena(int id_knjige, int id_korisnika, int ocena)
        {
            OceneKorisnika oObj = new OceneKorisnika();

            oObj.ID_Knjige = id_knjige;
            oObj.ID_Korisnika = id_korisnika;
            oObj.Ocena = ocena;

            if (!CheckOcena(oObj))
                return RedirectToAction("DisplayOceneKorisnikaData");

            try
            {
                session.Execute("INSERT INTO ocenekorisnika (id_knjige, id_korisnika, ocena) " +
                    "VALUES (" + oObj.ID_Knjige +
                    ", " + oObj.ID_Korisnika +
                    ", " + oObj.Ocena + ");");
            }
            catch (Exception ex)
            {

            }
            RacunajProsekOcena(id_knjige);
            RacunajProsekAutora(id_knjige);

            return RedirectToAction("DisplayOceneKorisnikaData");
        }

        private void RacunajProsekOcena(int id_knjige)
        {
            float suma = 0, n = 0;

            try
            {
                var ocene = session.Execute("Select ocena from ocenekorisnika where id_knjige=" + id_knjige + ";");

                foreach(var ocena in ocene)
                {
                    int ocn = ocena.GetValue<int>(0);
                    suma += ocn;
                    n++;
                }
                float prosek = suma / n;

                session.Execute("update knjiga set prosecna_ocena=" +
                                prosek.ToString("0.000", System.Globalization.CultureInfo.InvariantCulture) +
                                " where id_knjige=" + id_knjige + ";");
            }
            catch(Exception ex)
            { }
        }

        private void RacunajProsekAutora(int id_knjige)
        {
            float suma = 0, n = 0;
            try
            {
                var autor = session.Execute("select id_autora from knjigeautora where id_knjige=" + id_knjige + ";");

                int id_autora = autor.First().GetValue<int>("id_autora");

                var knjige = session.Execute("select id_knjige from knjigeautora where id_autora=" + id_autora + ";");

                foreach(var knjiga in knjige)
                {
                    int ids_knjige = knjiga.GetValue<int>(0);

                    var result = session.Execute("select prosecna_ocena from knjiga where id_knjige=" + ids_knjige + ";").FirstOrDefault();

                    float ocena = result.GetValue<float>(0);

                    suma += ocena;
                    n++;
                }

                float prosek = suma / n;

                session.Execute("update autor set prosecna_ocena=" +
                                prosek.ToString("0.000",System.Globalization.CultureInfo.InvariantCulture) +
                                " where id_autora=" +
                                id_autora + ";");
            }
            catch(Exception ex)
            { }
        }
    }
}