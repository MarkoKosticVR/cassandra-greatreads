﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra_GreatReads.Models;
using Cassandra;
using System.Globalization;


namespace Cassandra_GreatReads.Controllers
{
    public class KnjigaDataController : Controller
    {
        ISession session = null;

        public KnjigaDataController()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                session = cluster.Connect("greatreads");
            }
            catch (Exception ex)
            { }
        }
        public IActionResult DisplayKnjigaData()
        {
            KnjigaData kd = new KnjigaData();
            return View(kd);
        }

        public IActionResult GoToCreateKnjiga(int  id_knjige)
        {
            if (id_knjige != 0)
            {
                Knjiga tmpObj = null;

                try
                {
                    var results = session.Execute("SELECT * FROM knjiga WHERE " +
                                    "id_knjige =" + id_knjige + " ; ");

                    foreach (var result in results)
                    {
                        tmpObj = new Knjiga();

                        tmpObj.ID_Knjige = result.GetValue<int>("id_knjige");
                        tmpObj.Naslov = result.GetValue<String>("naslov");
                        tmpObj.Naslov_originala = result.GetValue<String>("naslov_originala");
                        tmpObj.Autor = result.GetValue<String>("autor");
                        tmpObj.Broj_stranica = result.GetValue<int>("broj_stranica");
                        tmpObj.Format_knjige = result.GetValue<String>("format_knjige");
                        tmpObj.Godina_izdavanja = result.GetValue<LocalDate>("godina_izdavanja");
                        tmpObj.Jezik = result.GetValue<String>("jezik");
                        tmpObj.Prosecna_ocena = result.GetValue<float>("prosecna_ocena");
                        tmpObj.Zanr = result.GetValue<String>("zanr");
                    }
                }
                catch (Exception ex) { }

                return View(tmpObj);
            }
            else
                return View();
        }

        private Boolean CheckKnjiga(Knjiga knjiga)
        {
            if (knjiga.ID_Knjige == 0)
                return false;
            return true;
        }

        public IActionResult SaveKnjiga(int id_knjige, String naslov, String naslov_originala, String autor, DateTime godina_izdavanja,
            String format_knjige, String jezik, String zanr, int broj_stranica, float prosecna_ocena)
        {
            Knjiga kObj = new Knjiga();

            kObj.ID_Knjige = id_knjige;
            kObj.Naslov = naslov;
            kObj.Naslov_originala = naslov_originala;
            kObj.Autor = autor;
            kObj.Godina_izdavanja = new LocalDate(godina_izdavanja.Year,godina_izdavanja.Month,godina_izdavanja.Day);
            kObj.Format_knjige = format_knjige;
            kObj.Jezik = jezik;
            kObj.Zanr = zanr;
            kObj.Broj_stranica = broj_stranica;
            kObj.Prosecna_ocena = prosecna_ocena;

            if(!CheckKnjiga(kObj))
                return RedirectToAction("DisplayKnjigaData");

            LocalDate localDate = null;
            LocalDate minLD = new LocalDate(1, 1, 1);
            if (kObj.Godina_izdavanja == minLD)
                localDate = new LocalDate(1000, 10, 10);
            else
                localDate = new LocalDate(kObj.Godina_izdavanja.Year, kObj.Godina_izdavanja.Month, kObj.Godina_izdavanja.Day);
            
            try
            {
                session.Execute("INSERT INTO knjiga (id_knjige, naslov, naslov_originala, autor, godina_izdavanja," +
                                "format_knjige, jezik, zanr, broj_stranica, prosecna_ocena)" +
                                "VALUES (" + kObj.ID_Knjige +
                                ", \'" + kObj.Naslov + "\'" +
                                ", \'" + kObj.Naslov_originala + "\'" +
                                ", \'" + kObj.Autor + "\'" +
                                ", \'" + localDate.ToString() + "\'" +
                                ", \'" + kObj.Format_knjige + "\'" +
                                ", \'" + kObj.Jezik + "\'" +
                                ", \'" + kObj.Zanr + "\'" +
                                ", " + kObj.Broj_stranica +
                                ", " + kObj.Prosecna_ocena + ");");
            }
            catch(Exception ex) { }
            return RedirectToAction("DisplayKnjigaData");
        }

        public IActionResult DeleteKnjiga(int id_knjige)
        {
            try
            {
                session.Execute("DELETE FROM knjiga WHERE " +
                                "id_knjige =" + id_knjige + " ; ");
            }
            catch(Exception ex) { }

            return RedirectToAction("DisplayKnjigaData");
        }

        public IActionResult UpdateKnjiga(int id_knjige)
        {
            return RedirectToAction("GoToCreateKnjiga", new { id_knjige = id_knjige });
        }
    }
}
