﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cassandra_GreatReads.Models;
using Cassandra;

namespace Cassandra_GreatReads.Controllers
{
    public class KorisnikController : Controller
    {
        ISession session = null;

        public KorisnikController()
        {
            try
            {
                var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
                session = cluster.Connect("greatreads");
            }
            catch (Exception ex)
            { }
        }
        public IActionResult DisplayKorisnikData()
        {
            KorisnikData kor = new KorisnikData();
            return View(kor);
        }

        public IActionResult GoToCreateKorisnik(int id_korisnika)
        {
            if (id_korisnika != 0)
            {
                Korisnik kor=null;
                try
                {
                    var results = session.Execute("SELECT * FROM korisnik WHERE " +
                                    "id_korisnika =" + id_korisnika + " ; ");
            
                    foreach (var rez in results)
                    {
                        kor = new Korisnik();
                        kor.ID_Korisnika = rez.GetValue<int>("id_korisnika");
                        kor.Email = rez.GetValue<string>("email");
                        kor.God_rodjenja = rez.GetValue<LocalDate>("godina_rodjenja");
                        kor.Ime_i_prezime = rez.GetValue<string>("ime_i_prezime");
                        kor.Lozinka = rez.GetValue<string>("lozinka");
                        kor.Pol = rez.GetValue<string>("pol");
                        kor.Username = rez.GetValue<string>("username");

                    
                    }

                }
                catch (Exception ex) { }
                return View(kor);
            }
            else
                return View();
        }


        public bool CheckKorisnik(Korisnik k)
        {
            if (k.ID_Korisnika == 0)
                return false;
            return true;
        }

        public IActionResult SaveKorisnik(int id_korisnika, string email, DateTime godina_rodjenja, string ime_i_prezime
            , string lozinka, string pol, string username)
        {
            Korisnik kor = new Korisnik();
            kor.ID_Korisnika = id_korisnika;
            kor.Email = email;
            kor.God_rodjenja = new LocalDate(godina_rodjenja.Year, godina_rodjenja.Month, godina_rodjenja.Day);
            kor.Ime_i_prezime = ime_i_prezime;
            kor.Lozinka = lozinka;
            kor.Pol = pol;
            kor.Username = username;
            
            if(!CheckKorisnik(kor))
                return RedirectToAction("DisplayKorisnikData");
            LocalDate localDate = null;
            LocalDate minLD = new LocalDate(1, 1, 1);
            if (kor.God_rodjenja == minLD)
                localDate = new LocalDate(1000, 10, 10);
            else
                localDate = new LocalDate(kor.God_rodjenja.Year, kor.God_rodjenja.Month, kor.God_rodjenja.Day);

            try
            {              
                session.Execute("INSERT INTO korisnik(id_korisnika, email, godina_rodjenja, ime_i_prezime, lozinka, pol, username)" +
                                "VALUES (" + kor.ID_Korisnika +
                                ", \'" + kor.Email + "\'" +
                                ", \'" + localDate.ToString() + "\'" +
                                ", \'" +kor.Ime_i_prezime + "\'" +
                                ", \'" + kor.Lozinka + "\'" +
                                ", \'" + kor.Pol + "\'"+
                                ",\' " + kor.Username + "\');");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }
            return RedirectToAction("DisplayKorisnikData");

        }


        public IActionResult DeleteKorisnik (int id_korisnika)
        {
            try
            {
                session.Execute("DELETE FROM korisnik WHERE " +
                                "id_korisnika = " + id_korisnika + " ; ");
            }
            catch (Exception ex) { Console.WriteLine(ex.Message); }

            return RedirectToAction("DisplayKorisnikData");
        }

        public IActionResult UpdateKorisnik (int id_korisnika)
        {
            return RedirectToAction("GoToCreateKorisnik", new { id_korisnika = id_korisnika });
        }

        public IActionResult PogledajPolice (int id_korisnika)
        {
            return RedirectToAction("DisplayPolicaData", "Polica", new { id_korisnika = id_korisnika });
        }

    }
}
